var c = document.getElementById("game"),
  ctx = c.getContext("2d"),
  scoreText = document.getElementById("score");

c.height = window.innerHeight - document.getElementById("head").offsetHeight;
c.width = document.getElementById("game").offsetWidth;

const TILE_START_SPEED = 0.2;

const TILE_WIDTH = c.clientWidth / 4 - 0.5;
const TILE_HEIGHT = c.clientHeight / 5;

var lastRow = [];
var rows = [];

var endScreen = document.getElementById("end-screen");

//ALL ABOUT ROWS
//
//
function drawTile(x, y, deadly, hit) {
  var color = "";
  if (deadly) {
    if (hit) color = "#D77";
    else color = "#DDD";
  } else {
    if (hit) color = "#666";
    else color = "#222";
  }
  ctx.beginPath();
  ctx.rect(x, y, TILE_WIDTH, TILE_HEIGHT);
  ctx.fillStyle = color;
  ctx.fill();
  ctx.lineWidth = 2;
  ctx.strokeStyle = "#AAA";
  ctx.stroke();
}
function createRow(blackTile) {
  var offset = 0;
  if (lastRow[0]) offset = lastRow[0].position.y;
  lastRow = [];
  for (var i = 0; i < 4; i++) {
    if (i != blackTile)
      lastRow.push({
        position: { x: TILE_WIDTH * i + 1, y: -TILE_HEIGHT + offset },
        deadly: true,
        hit: false
      });
    else
      lastRow.push({
        position: { x: TILE_WIDTH * i + 1, y: -TILE_HEIGHT + offset },
        deadly: false,
        hit: false
      });
  }
  rows.push(lastRow);
}

function drawRows() {
  rows.forEach(row => {
    row.forEach(tile => {
      drawTile(tile.position.x, tile.position.y, tile.deadly, tile.hit);
    });
  });
}

function moveTiles(deltaTime, speedMultiplier) {
  if (deltaTime >= 60) deltaTime = 0;
  rows.forEach(row => {
    row.forEach(tile => {
      tile.position.y =
        tile.position.y + TILE_START_SPEED * speedMultiplier * deltaTime;
    });
  });
}

function clearInvisibleTiles() {
  if (rows[0][0].position.y >= c.height) {
    rows[0].forEach(tile => {
      if (!tile.deadly && !tile.hit) lose();
    });
    rows.shift();
  }
}

//CANVAS
//
//
function clearCanvas() {
  ctx.clearRect(0, 0, c.width, c.height);
}

function handleTileClick(e) {
  e.preventDefault();
  if (e.type == "click") {
    var x = e.pageX - c.offsetLeft,
      y = e.pageY - c.offsetTop;
  } else {
    var x = e.changedTouches[0].pageX - c.offsetLeft,
      y = e.changedTouches[0].pageY - c.offsetTop;
  }

  var column = Math.floor(x / TILE_WIDTH);

  rows.forEach((row, index) => {
    if (
      y > row[column].position.y &&
      y < row[column].position.y + TILE_HEIGHT
    ) {
      rows[index][column].hit = true;
      if (rows[index][column].deadly) lose();
      else {
        score++;
        scoreText.textContent = "Score: " + score;
      }
    }
  });
}

c.addEventListener("click", handleTileClick, false);
c.addEventListener("touchstart", handleTileClick, false);

//GAME LOGIC
//
//
var updateInterval;
var score = 0;
var timerStart = Date.now();
var time = timerStart;
var alive = true;
var deltaTime = 0;
var multiplier = 1;

function lose() {
  alive = false;
  c.removeEventListener("click", handleTileClick);
  c.removeEventListener("touchstart", handleTileClick);
  clearInterval(updateInterval);
  clearCanvas();
  drawRows();
  document.getElementById("end-score").textContent = "Your Score: " + score;
}

function restart() {
  rows = [];
  clearCanvas();
  alive = true;
  timerStart = Date.now();
  c.addEventListener("click", handleTileClick, false);
  c.addEventListener("touchstart", handleTileClick, false);
  document.getElementById("end-screen").style.display = "none";
  createRow(Math.floor(Math.random() * 4));
  requestAnimationFrame(update);
}

function update() {
  deltaTime = Date.now() - time;
  time = Date.now();
  multiplier = (Date.now() - timerStart) / 50000 + 1;
  moveTiles(deltaTime, multiplier);

  if (rows[rows.length - 1][0].position.y >= -3)
    createRow(Math.floor(Math.random() * 4));
  clearCanvas();
  drawRows();
  clearInvisibleTiles();
  if (alive) requestAnimationFrame(update);
  else document.getElementById("end-screen").style.display = "block";
}

function main() {
  document.getElementById("end-screen").style.display = "none";
  createRow(Math.floor(Math.random() * 4));
  requestAnimationFrame(update);
}

main();
